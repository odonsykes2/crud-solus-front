import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http'; 
import {Pessoa} from '../services/pessoa';
import {ConfigService} from './config.service';
import { Observable } from 'rxjs';



 // informa na propria anotacao de onde ele vai ser 'provido'

    @Injectable({providedIn: 'root'})// vai ser provido pelo app.module
    export class PessoaService {
     
        private baseUrlService:string = '';
        private headers = new HttpHeaders({'Content-Type': 'application/json'});
        private options;
     
        constructor(private http: HttpClient,
                    private configService: ConfigService) { 
     
            /**SETANDO A URL DO SERVIÇO REST QUE VAI SER ACESSADO */
            this.baseUrlService = configService.getUrlService() + '/pessoa/';
     
            /*ADICIONANDO O JSON NO HEADER */

            this.options = { headers: this.headers };
        }
     
        /**CONSULTA TODAS AS PESSOAS CADASTRADAS */
        getPessoas(){        
            return this.http.get<Pessoa[]>(this.baseUrlService, {headers: this.headers});
        }
     
        /**ADICIONA UMA NOVA PESSOA */
        addPessoa(pessoa: Pessoa){
     
            return this.http.post(this.baseUrlService, JSON.stringify(pessoa), {headers: this.headers});
        }
        /**EXCLUI UMA PESSOA */
        excluirPessoa(codigo:number){
     
            return this.http.delete(this.baseUrlService + codigo, {headers: this.headers});
        }
     
        /**CONSULTA UMA PESSOA PELO CÓDIGO */
        getPessoa(codigo:number){
     
            return this.http.get<Pessoa>(this.baseUrlService + codigo, {headers: this.headers});
        }
     
        /**ATUALIZA INFORMAÇÕES DA PESSOA */
        atualizarPessoa(pessoa:Pessoa){
            // não é obrigatorio usar o stringfy
            return this.http.put(this.baseUrlService, pessoa, {headers: this.headers});
        }
     
    }