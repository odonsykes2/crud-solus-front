import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TesteComponent } from './teste/teste.component';

import { MenuComponent } from './Menu/menu.component'
import { HomeComponent } from './home/home.component'
import { CadastroComponent } from './home/pessoa/cadastro/cadastro.component';

import { PessoaService} from './services/pessoa.service';
import { ConsultaComponent } from './home/pessoa/consulta/consulta.component';

@NgModule({
  declarations: [
    AppComponent,
    TesteComponent,
    CadastroComponent,
    MenuComponent,
    HomeComponent,
    ConsultaComponent,
    CadastroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  // no angular 8 não precisa declarar os servicos
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
